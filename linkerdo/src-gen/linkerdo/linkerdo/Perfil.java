/**
 */
package linkerdo.linkerdo;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perfil</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link linkerdo.linkerdo.Perfil#getNome <em>Nome</em>}</li>
 *   <li>{@link linkerdo.linkerdo.Perfil#getPage <em>Page</em>}</li>
 * </ul>
 *
 * @see linkerdo.linkerdo.LinkerdoPackage#getPerfil()
 * @model
 * @generated
 */
public interface Perfil extends EObject {
	/**
	 * Returns the value of the '<em><b>Nome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nome</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nome</em>' attribute.
	 * @see #setNome(String)
	 * @see linkerdo.linkerdo.LinkerdoPackage#getPerfil_Nome()
	 * @model
	 * @generated
	 */
	String getNome();

	/**
	 * Sets the value of the '{@link linkerdo.linkerdo.Perfil#getNome <em>Nome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nome</em>' attribute.
	 * @see #getNome()
	 * @generated
	 */
	void setNome(String value);

	/**
	 * Returns the value of the '<em><b>Page</b></em>' containment reference list.
	 * The list contents are of type {@link linkerdo.linkerdo.Page}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page</em>' containment reference list.
	 * @see linkerdo.linkerdo.LinkerdoPackage#getPerfil_Page()
	 * @model containment="true"
	 * @generated
	 */
	EList<Page> getPage();

} // Perfil
