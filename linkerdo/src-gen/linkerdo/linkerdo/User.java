/**
 */
package linkerdo.linkerdo;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link linkerdo.linkerdo.User#getNome <em>Nome</em>}</li>
 *   <li>{@link linkerdo.linkerdo.User#getPerfil <em>Perfil</em>}</li>
 * </ul>
 *
 * @see linkerdo.linkerdo.LinkerdoPackage#getUser()
 * @model
 * @generated
 */
public interface User extends EObject {
	/**
	 * Returns the value of the '<em><b>Nome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nome</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nome</em>' attribute.
	 * @see #setNome(String)
	 * @see linkerdo.linkerdo.LinkerdoPackage#getUser_Nome()
	 * @model
	 * @generated
	 */
	String getNome();

	/**
	 * Sets the value of the '{@link linkerdo.linkerdo.User#getNome <em>Nome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nome</em>' attribute.
	 * @see #getNome()
	 * @generated
	 */
	void setNome(String value);

	/**
	 * Returns the value of the '<em><b>Perfil</b></em>' containment reference list.
	 * The list contents are of type {@link linkerdo.linkerdo.Perfil}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Perfil</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Perfil</em>' containment reference list.
	 * @see linkerdo.linkerdo.LinkerdoPackage#getUser_Perfil()
	 * @model containment="true"
	 * @generated
	 */
	EList<Perfil> getPerfil();

} // User
