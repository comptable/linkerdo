# Descrição do Projeto LINKERDO
### UNIVERSIDADE FEDERAL DO CEARÁ
### Disciplina de Tópicos Avançados de Engenharia de Software

##### AUTOR: RUBENS ANDERSON DE SOUSA SILVA

### INTRODUÇÃO

 Este plug-in foi desenvolvido para a plataforma Eclipse em um projeto da disciplina de Tópicos Avançados em Engenharia de Software, ministrada pelo Professor Doutor [João Bosco Ferreira Filho][lates do bosco], em Universidade Federal do Ceará, sétimo semestre do curso de Ciência da Computação (2018.1).

### MOTIVAÇÃO

Sempre que mudamos de computador, é necessário reabrir várias abas de navegador para poder voltar ao que estava fazendo, seja trabalho ou lazer.

Então uma forma de minimizar esse tempo é fazer com que as abas possam ser reabertas de forma mais simples, com poucos cliques. O usuário cadastra seus sites e vincula-os com perfis. Tudo isso de forma fácil, com interface drag-n-drop. Quando quiser abrir suas abas cadastradas, basta abrir a interface HTML gerada e apertar botões para abrir as novas guias.

Este projeto foi desenvolvido para melhorar o desempenho de quem trabalha de forma distribuida, com vários locais de trabalho e que depende de vários sites para trabalhar, ou de quem só quer poupar tempo. 

### DESCRIÇÃO 

Este projeto utilizou uma série de plug-ins do eclipse para ajudar na manutenção desenvolvimento do projeto. Foi utilizado o Sirius (see [sirius web page][Sirius]) e o ECORE para desenvolver a DSL, Acceleo (see [Acceleo web page][Acceleo]) como compilador do que foi produzido pelo usuário e o próprio Eclipse como ferramenta de desenvolvimento.


Foi desenvolvido em dois meses com carga horária de 4 horas por semana. Ao que se foi proposto para a disciplina, posso considerar um sucesso.

### IMAGENS DA APLICAÇÃO


Eis a interface drag-n-drop da aplicação.

![https://drive.google.com/open?id=11XDYFNp2KcmXZEqt0eTd_sldP53GydKv][dragndrop]


Aqui, está o HTML gerado. 

![https://drive.google.com/open?id=1QuxOxwZemmkr7e-vxcdGpv3gV6688Rzw][html]


Aqui, o metamodelo da aplicação. É, de fato, bem simples. 

![https://drive.google.com/open?id=1Q-n431A4eXeanWOLmrBvqH2fUabELQeA][metamodelo]


### PRÓXIMOS PASSOS

Os próximos passos previstos são: Criar uma extensão para navegadores com a interface drag-n-drop, e a possibilidade de salvar os perfis em nuvem. Assim, um usuário poderia recuperar suas páginas de forma mais fácil, sem depender do Eclipse, aumentando a portabilidade e a praticidade da aplicação. 

### COMO TESTAR

É necessário ter em seu computador a versão mais recente do Eclipse, e instalados nela os plug-ins do Sirius, ECORE.

Primeiro, deve-se baixar todos os arquivos que não são imagens. Logo após, deve-se importar o projeto com todas as pastas, exceto **a pasta linkerdo_v2**, pois esta deve ser importada na instância dinâmica. É nela que reside a interface com o usuário final.

Para gerar o código do HTML, basta clicar com o botão direito sobre o arquivo.linkerdo e clicar em 
>_Acceleo Model to Text / Generate Linkerdo_

O arquivo será exportado para a pasta **_src-gen_** do projeto.

Para mais informações de como usar o Sirius e o Acceleo, acesse a página do [Sirius][Sirius] e [Acceleo][Acceleo] na sessão de tutoriais.

### CONTATO

* Email: rubsandsu@gmail.com
* instagram: @rubensandersonn
* Facebook: facebook.com/rubensanderson23
* github: github.com/rubensandersonn









[html]:https://drive.google.com/open?id=1QuxOxwZemmkr7e-vxcdGpv3gV6688Rzw

[dragndrop]:https://drive.google.com/open?id=11XDYFNp2KcmXZEqt0eTd_sldP53GydKv

[metamodelo]:https://drive.google.com/open?id=1Q-n431A4eXeanWOLmrBvqH2fUabELQeA



[Sirius]:www.eclipse.org/sirius/getstarted.html

[Acceleo]:www.eclipse.org/acceleo/getstarted.html

[lates do bosco]:(http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4230989A1)